import React from 'react';
import { AppRegistry, StatusBar } from 'react-native';
import { Container, Content, Text, List, ListItem } from 'native-base';
const routes = [{ name: 'Inicio', route: 'Home' }];

export default class Sidebar extends React.Component {
  render() {
    return (
      <Container>
        <Content>
          <List
            dataArray={routes}
            renderRow={({ name, route }) => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(route)}
                >
                  <Text>{name}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}
