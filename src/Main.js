import React from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Left,
  Right,
  Body,
  Icon,
} from 'native-base';

const Main = ({ openDrawer, children }) => {
  return (
    <Container>
      {/* Header */}
      <Header>
        <Left>
          <Button transparent onPress={openDrawer}>
            <Icon name="menu" />
          </Button>
        </Left>
        <Body>
          <Title>Carta de logros</Title>
        </Body>
        <Right />
      </Header>

      {/* Content */}
      <Content>{children}</Content>
    </Container>
  );
};

export default Main;
