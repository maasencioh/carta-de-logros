import React from 'react';
import { createDrawerNavigator } from 'react-navigation';

import Home from './Home';
import Sidebar from './Sidebar';

const Router = createDrawerNavigator(
  {
    Home: { screen: Home },
  },
  {
    initialRouteName: 'Home',
    contentOptions: { activeTintColor: '#e91e63' },
    contentComponent: (props) => <Sidebar {...props} />,
  }
);

export default Router;
