import React, { Component } from 'react';
import { Text } from 'native-base';

import Main from './Main';

export default class Home extends Component {
  static navigationOptions = {
    drawerLabel: 'Home',
  };

  render() {
    return (
      <Main openDrawer={() => this.props.navigation.openDrawer()}>
        <Text>Hola</Text>
      </Main>
    );
  }
}
